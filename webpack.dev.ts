import merge from 'webpack-merge'
import commonConfig from './webpack.common'
import webpack from 'webpack'

const devConfig: webpack.Configuration = merge(commonConfig, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './src',
    hot: true,
  },
})

export default devConfig
