System.config({
  paths: {
    ">/*": "./*",
    "~/*": "./src/*",
    "#/*": "./src/utils/*",
    "=/*": "./src/components/*",
    "@/*": "./src/Pages/*",
    "+/*": "./src/styles/*",
    "-/*": "./src/static/*"
  }
})
