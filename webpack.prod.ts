import merge from 'webpack-merge'
import commonConfig from './webpack.common'
import webpack from 'webpack'

const prodConfig: webpack.Configuration = merge(commonConfig, {
  mode: 'production',
})

export default prodConfig
