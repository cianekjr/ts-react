import path from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import webpack from 'webpack'
import { CleanWebpackPlugin } from 'clean-webpack-plugin'
import { InjectManifest } from 'workbox-webpack-plugin'
import WebpackPwaManifest from 'webpack-pwa-manifest'

const commonConfig: webpack.Configuration = {
  entry: './src/Root/index.tsx',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  resolve: {
    extensions: ['*', '.ts', '.tsx', '.js', '.jsx'],
  },
  target: 'web',
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.(png|jpe?g|svg|gif|ico)$/,
        exclude: /node_modules/,
        use: {
          loader: 'file-loader',
        },
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src', 'Root', 'index.html'),
      favicon: './src/static/img/favicon.ico',
    }),
    new InjectManifest({
      swSrc: './src/Root/service-worker.js',
      swDest: 'service-worker.js',
      include: [/\.(html|js)$/],
      importWorkboxFrom: 'cdn',
    }),
    new WebpackPwaManifest({
      filename: 'manifest.json',
      name: 'Test',
      short_name: 'Test',
      description: 'Test app',
      start_url: '/',
      scope: '/',
      theme_color: '#5bbc5f',
      background_color: '#313131',
      display: 'standalone',
      crossorigin: 'use-credentials',
      inject: true,
      fingerprints: true,
      ios: {
        'apple-mobile-web-app-title': 'Test',
        'apple-mobile-web-app-status-bar-style': 'black-translucent',
      },
      icons: [
        {
          src: path.resolve('./src/static/img/icon.png'),
          sizes: [72, 96, 128, 144, 152, 192, 256, 384, 512],
          destination: 'icons',
          ios: true,
        },
      ],
    }),
  ],
}

export default commonConfig
