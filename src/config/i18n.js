import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import Backend from 'i18next-xhr-backend'
import LanguageDetector from 'i18next-browser-languagedetector'

const initFn = () =>
  i18n
    .use(Backend)
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
      lng: 'pl',
      fallbackLng: 'pl',
      ns: ['commons'],
      load: 'currentOnly',
      debug: false,
      backend: {
        loadPath: '../public/locales/{{lng}}/{{ns}}.json',
        allowMultiLoading: false
      }
    })

export default initFn
