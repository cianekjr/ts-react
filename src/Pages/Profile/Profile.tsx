import React, { FunctionComponent } from 'react'
import { useHistory } from 'react-router'

const Index: FunctionComponent = () => {
  const history = useHistory()
  return (
    <div>
      Profile
      <button onClick={() => history.goBack()}>Back</button>
      <button onClick={() => history.push('/')}>Index</button>
    </div>
  )
}

export default Index
