import React, { FunctionComponent } from 'react'
import { useHistory } from 'react-router'

import { useTranslation } from 'react-i18next'
import { Button } from './Dashboard.shards'

type HelloProps = {
  name: string;
  age?: number;
};

const Hello: FunctionComponent<HelloProps> = ({ name, age }) => {
  const history = useHistory()
  const { t } = useTranslation('commons')
  return (
    <div className='hello'>
      Hello {name}. {age && `You have ${age} years old!!!`}
      <Button onClick={() => history.goBack()}>Back</Button>
      <Button onClick={() => history.push('/profile')}>Profile</Button>
      <div>{t('test')}</div>
    </div>
  )
}

export default Hello
