// @ts-nocheck
if (self.workbox) {
  self.workbox.setConfig({
    debug: false
  })

  self.workbox.routing.registerRoute(
    new RegExp('/*'),
    new self.workbox.strategies.StaleWhileRevalidate({
      cacheName: 'CACHE_STATIC-v1.0',
      plugins: [
        new self.workbox.expiration.Plugin({
          maxEntries: 60,
          maxAgeSeconds: 30 * 24 * 60 * 60 // 30 Days
        })
      ]
    })
  )
}
