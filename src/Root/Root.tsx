import React, { FunctionComponent, Suspense } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Index from '@/Dashboard'
import Profile from '@/Profile'

const App: FunctionComponent = () => (
  <Suspense fallback={<div>Loading...</div>}>
    <BrowserRouter basename='/#'>
      <Switch>
        <Route exact path='/'>
          <Index name='Matthieu' age={23} />
        </Route>
        <Route exact path='/profile'>
          <Profile />
        </Route>
      </Switch>
    </BrowserRouter>
  </Suspense>
)

export default App
