import React from 'react'
import ReactDOM from 'react-dom'

import Root from './Root'
import i18n from '~/config/i18n'

const register = false
if (register && 'serviceWorker' in navigator) {
  (async () => {
    await navigator.serviceWorker.register('service-worker.js')
  })()
}

i18n().then(() => {
  ReactDOM.render(<Root />, document.getElementById('root'))
})
